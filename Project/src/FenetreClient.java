
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author P14B-124-ALPHA
 */
public final class FenetreClient extends JFrame{
        final File[] fileToSend=new File[1];
        
    JLabel jlTitle=new JLabel("Send File");
        
        
        JLabel jlFileName=new JLabel("Choose file to send.");
       
        
        
        JPanel jpButton=new JPanel();
                
                
        JButton jbSendFile=new JButton("send File");
        

        JButton jbChooseFile=new JButton("Choose File");

    FenetreClient(String testcode_client) {
            super(testcode_client);
            iniComponents();
            
    }
        
        void iniComponents(){
            //prepa fenetre
         this.setSize(450, 450);
        this.setLayout(new BoxLayout(this.getContentPane(),BoxLayout.Y_AXIS));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //prepa titre
        jlTitle.setFont(new Font("Arial",Font.BOLD, 25));
        jlTitle.setBorder(new EmptyBorder(20, 0, 10,0));
        jlTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        // prepa file
         jlFileName.setFont(new Font("Arial",Font.BOLD, 20));
        jlFileName.setBorder(new EmptyBorder(50, 0, 0,0));
        jlFileName.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //panel de button
        jpButton.setBorder(new EmptyBorder(75, 0, 10,0));
        //button send
        jbSendFile.setPreferredSize(new Dimension(150, 75));
        jbSendFile.setFont(new Font("Arial",Font.BOLD, 20));
        
        //button choix de fichier 
         jbChooseFile.setPreferredSize(new Dimension(150, 75));
        jbChooseFile.setFont(new Font("Arial",Font.BOLD, 25));
        
        //ajout des button send et choix de fichier dans le panel jpButton
        jpButton.add(jbSendFile);
        jpButton.add(jbChooseFile);
        //gestion des listener
        listenerDeButton();
        //ajout des composants dans le frame
        this.add(jlTitle);
        this.add(jlFileName);
        this.add(jpButton);
        }

    public FenetreClient() {
    iniComponents();
    
    }

    private void listenerDeButton() {
    jbChooseFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    JFileChooser jFileChooser=new JFileChooser();
                    jFileChooser.setDialogTitle("choose file to send");
                    
                    if (jFileChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
                        fileToSend[0]=jFileChooser.getSelectedFile();
                        jlFileName.setText("the file you want to send is "+fileToSend[0].getName());
                }
            }
        });
        
        jbSendFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (fileToSend[0]==null) {
                    jlFileName.setText("please choose a file first");
                }
                else{
                    
                    try {
                        
                    FileInputStream fileInputStream= new FileInputStream(fileToSend[0].getAbsolutePath());
                    Socket socket=new Socket("localhost", 1234);
                    DataOutputStream dataOutputStream= new DataOutputStream(socket.getOutputStream());
                    String fileName= fileToSend[0].getName();
                    byte[] filNameBytes=fileName.getBytes();
                    
                    byte[] fileContentBytes= new byte[(int)fileToSend[0].length()]; 
                    fileInputStream.read(fileContentBytes);
                    
                    dataOutputStream.writeInt(filNameBytes.length);
                    dataOutputStream.write(filNameBytes);
                    
                    dataOutputStream.writeInt(fileContentBytes.length);
                    dataOutputStream.write(fileContentBytes);
                    
                        System.out.println("lasa");

                    
                    } catch (IOException error) {
                        System.out.println("error: "+error.getMessage());
                    }
                    finally{
                        
                    }
                }
            }
        });
    }
        
}
