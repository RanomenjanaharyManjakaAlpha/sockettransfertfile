/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author P14B-124-ALPHA
 */
public final class FenetreServer extends JFrame{
         static  ArrayList<MyFile> myFiles=new ArrayList<>();
         int fileid=0;
         ServerSocket serverSocket= null; //socket serveur
          JPanel jPanel=new  JPanel();

    public FenetreServer() throws HeadlessException, IOException {
                initComponents();
    }    

    FenetreServer(String server) throws IOException {
    initComponents();
    }
          
          
         void receiveFile(){
          while (true) {          
             
             try {
                 Socket socket = serverSocket.accept();
                
                 DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                 int fileNameLength=dataInputStream.readInt();
                 if (fileNameLength>0) {
                     byte[] fileNameBytes=new byte[fileNameLength];
                     dataInputStream.readFully(fileNameBytes, 0,fileNameBytes.length);
                     String fileName=new  String(fileNameBytes);
                     
                     int fileContentLength=dataInputStream.readInt();
                     
                     if (fileContentLength>0) {
                         byte[] fileContentBytes=new byte[fileContentLength];
                            dataInputStream.readFully(fileContentBytes,0,fileContentLength);
                         
                         JPanel jpFileRow=new JPanel();
                         jpFileRow.setLayout(new BoxLayout(jpFileRow,BoxLayout.Y_AXIS));
                         
                         JLabel jlFileName=new JLabel(fileName);
                         jlFileName.setFont(new Font("Arial", Font.BOLD, 20));
                         jlFileName.setBorder(new EmptyBorder(10, 0, 10, 0));
                         
                         if (getFileExtension(fileName).equalsIgnoreCase("txt")) {
                             jpFileRow.setName(String.valueOf(fileid));
                             jpFileRow.addMouseListener(getMouseListener());
                             
                             jpFileRow.add(jlFileName);
                             jPanel.add(jpFileRow);
                             
                             this.validate();
                             
                         }else{
                             jpFileRow.setName(String.valueOf(fileid));
                             jpFileRow.addMouseListener(getMouseListener());
                             
                             jpFileRow.add(jlFileName);
                             jPanel.add(jpFileRow);
                             
                             this.validate();
                         }
                         System.out.println("test");
                 myFiles.add(new MyFile(fileid, fileName,fileContentBytes, getFileExtension(fileName)));
                     }
                 }
                  
                 
             } catch (IOException e) {
                 System.out.println("error: "+e.getMessage());
             }
        }}
         
        private void initComponents() throws IOException{
            System.out.println("transfertfichier.FenetreServer.initComponents()");
         JScrollPane jScrollPane=new JScrollPane(jPanel); //scroll
         JLabel jlTitle=new JLabel("file receiver"); //titre 
         
         //preparation du fenetre
            this.setSize(400, 400);
            this.setLayout(new BoxLayout(this.getContentPane(),BoxLayout.Y_AXIS));
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            //preparation ...
            jPanel.setLayout(new BoxLayout(jPanel,BoxLayout.Y_AXIS));
            
            //preparation du scroolbar
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            //prepa du titre
            jlTitle.setFont(new Font("Arial", Font.BOLD, 25));
            jlTitle.setBorder(new EmptyBorder(20, 0, 10, 0));
            jlTitle.setAlignmentX(Component.CENTER_ALIGNMENT);


            //ajout des composant dans la fenetre
            this.add(jlTitle);
            this.add(jScrollPane);
            //instanciation du serveur socket
            serverSocket= new ServerSocket(1234);
            setVisible(true);
            receiveFile();
         }
         //fonction generer un mouselistener pour selectionner un fichier dans le server pour telecharger
        public static MouseListener getMouseListener(){
            return new MouseListener(){
                @Override
                public void mouseClicked(MouseEvent e) {
                   JPanel jPanel =(JPanel)e.getSource();
                   int fileid=Integer.parseInt(jPanel.getName());

                    for (MyFile myFile : myFiles) {
                        if (myFile.getId()==fileid) {
                            JFrame jPreview= createFrame(myFile.getName(),myFile.getData(),myFile.getFileExtension());
                            jPreview.setVisible(true);
                        }
                    }
                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            };//instance mouseListener mihitsy ity
    }
        //fenetre de dialogue si vouloir vraiment telecharger le fichier
    public static JFrame createFrame(String filename,byte[]fileData,String fileExtension){
        JFrame jFrame=new JFrame("file downloader");
        jFrame.setSize(400,400);
        
        JPanel jPanel=new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
        
        JLabel jlTitle=new JLabel("file downloader");
        jlTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlTitle.setFont(new Font("Arial", Font.BOLD,25));
        jlTitle.setBorder(new EmptyBorder(20,0,10,0));
        
        JLabel jlPrompt=new JLabel("Are you sure you want to download "+filename);
        jlPrompt.setFont(new Font("Arial", Font.BOLD,20));
        jlPrompt.setBorder(new EmptyBorder(20,0,10,0));
        jlPrompt.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JButton jbYes=new JButton("Yes");
        jbYes.setPreferredSize(new Dimension(150, 75));
        jbYes.setFont(new Font("Arial", Font.BOLD, 20));
        
        JButton jbNo=new JButton("NO");
        jbNo.setPreferredSize(new Dimension(150, 75));
        jbNo.setFont(new Font("Arial", Font.BOLD, 20));
        
        JLabel jlFileContent=new JLabel();
        jlFileContent.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JPanel jpButton=new JPanel();
        jpButton.setBorder(new EmptyBorder(20, 0, 10, 0));
        jpButton.add(jbYes);
        jpButton.add(jbNo);
        
        if(fileExtension.equalsIgnoreCase("txt")){
            System.out.println(fileExtension);
            jlFileContent.setText("<html>"+new String(fileData)+"</html>"); 
        }
        else{
            jlFileContent.setIcon(new ImageIcon(fileData));
        }
        jbYes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File fileToDownload=new File(filename);
                try {
                    FileOutputStream fileOutputStream=new  FileOutputStream(fileToDownload);
                    fileOutputStream.write(fileData);
                    fileOutputStream.close();
                    
                    jFrame.dispose();
                } catch (IOException error) {
                    System.out.println("error: "+ error.getMessage());
                }
            }
        });
        jbNo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              jFrame.dispose();
            }});
                jPanel.add(jlTitle);
                jPanel.add(jlPrompt);
                jPanel.add(jlFileContent);
                jPanel.add(jpButton);
        
        jFrame.add(jPanel);
        return jFrame;
    }
        //maka extension an'ilay fichier
    public static  String getFileExtension(String filName){
        int i=filName.lastIndexOf('.');
            if (i>0) {
                return filName.substring(i+1);
            }else{

            return "no extension found";
            }
        }
    
}
